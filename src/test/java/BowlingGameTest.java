import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BowlingGameTest {
    BowlingGame bowlingGame;
    @Test
    void should_return_0_when_no_throws() {
        bowlingGame = new BowlingGame();
        assertEquals(0,bowlingGame.getTotalScore());
    }

    @Test
    void should_return_number_of_pins_knocked_down_when_have_1_thorw() {
        bowlingGame = new BowlingGame(Arrays.asList(8));
        bowlingGame. calculateTotalScore();
        assertEquals(8,bowlingGame.getTotalScore());
    }

    @Test
    void should_return_all_of_the_pins_knocked_down_when_have_2_throw() {
        bowlingGame = new BowlingGame(Arrays.asList(8,9));
        bowlingGame.calculateTotalScore();
        assertEquals(17,bowlingGame.getTotalScore());
    }

    @Test
    void should_return_total_score_when_have_10_frames_whithout_strike_and_spare() {
        List<Integer> throwsScores = Arrays.asList(2,3,7,1,7,2,8,1,5,2,2,3,7,1,7,1,8,1,5,2);
        bowlingGame = new BowlingGame(throwsScores);
        bowlingGame.calculateTotalScore();
        assertEquals(75,bowlingGame.getTotalScore());
    }

    @Test
    void should_return_total_score_when_the_first_frame_is_spare() {
        List<Integer> throwsScores = Arrays.asList(5,5,7,1,7,2,8,1,5,2,2,3,7,1,7,1,8,1,5,2);
        bowlingGame = new BowlingGame(throwsScores);
        bowlingGame.calculateTotalScore();
        assertEquals(87,bowlingGame.getTotalScore());

    }

    @Test
    void should_return_total_throw_pins_plus_spare_bonus_when_the_10th_frame_is_spare() {
        List<Integer> throwsScores = Arrays.asList(5,5,7,1,7,2,8,1,5,2,2,3,7,1,7,1,8,1,5,5,9);
        bowlingGame = new BowlingGame(throwsScores);
        bowlingGame.calculateTotalScore();
        assertEquals(99,bowlingGame.getTotalScore());
    }

    @Test
    void should_return_total_score_when_only_the_first_frame_is_strike() {
        List<Integer> throwsScores = Arrays.asList(10,0,7,1,7,2,8,1,5,2,2,3,7,1,7,1,8,1,5,5,9);
        bowlingGame = new BowlingGame(throwsScores);
        bowlingGame.calculateTotalScore();
        assertEquals(100,bowlingGame.getTotalScore());
    }
    @Test
    void should_return_total_score_when_the_last_frame_is_not_strike() {
        List<Integer> throwsScores = Arrays.asList(10,0,7,1,10,0,8,1,5,2,2,3,7,1,7,1,8,1,5,5,9);
        bowlingGame = new BowlingGame(throwsScores);
        bowlingGame.calculateTotalScore();
        assertEquals(110,bowlingGame.getTotalScore());
    }
    @Test
    void should_return_total_score_when_the_last_frame_is_strike() {
        List<Integer> throwsScores = Arrays.asList(10,0,7,1,7,2,8,1,5,2,2,3,7,1,7,1,8,1,10,0,9,9);
        bowlingGame = new BowlingGame(throwsScores);
        bowlingGame.calculateTotalScore();
        assertEquals(109,bowlingGame.getTotalScore());
    }
    @Test
    void should_return_300_when_it_is_a_perfect_game() {
        List<Integer> throwsScores = Arrays.asList(10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,10);
        bowlingGame = new BowlingGame(throwsScores);
        bowlingGame.calculateTotalScore();
        assertEquals(300,bowlingGame.getTotalScore());
    }


}