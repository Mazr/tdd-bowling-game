import java.util.ArrayList;
import java.util.List;

public class BowlingGame {
    private List<Integer> throwScores = new ArrayList();

    private List<Integer> frameScores = new ArrayList();


    private int totalScore = 0;

    public BowlingGame(List<Integer> throwScores) {
        this.throwScores.addAll(throwScores);
        if (this.throwScores.size() % 2 == 1)
            this.throwScores.add(0);
        setFrameScores();
    }

    public BowlingGame() {
    }

    private int getTotalKnockDownScores() {
        return throwScores.stream().limit(20).mapToInt(t -> t).sum();
    }

    void setFrameScores() {
        int length = Math.min(throwScores.size(), 20);
        for (int i = 0; i < length; i += 2) {
            frameScores.add(throwScores.get(i) + throwScores.get(i + 1));
        }
    }

    private int getTotalSpareBonus() {
        int spareBonus = 0;
        for (int i = 0; i < frameScores.size(); i++) {
            if (frameScores.get(i) == 10) {
                spareBonus += throwScores.get(i * 2 + 2);
            }
        }
        return spareBonus;
    }

    private int getTotalStrikeBonus() {
        int strikeBonus = 0;
        int length = Math.min(throwScores.size(), 20);
        for (int i = 0; i < length; i += 2) {
            if (throwScores.get(i) == 10) {
                if (throwScores.get(i + 2) == 10 && i != 18) {
                    strikeBonus += throwScores.get(i + 4);
                } else {
                    strikeBonus += throwScores.get(i + 3);
                }
            }
        }
        return strikeBonus;

    }

    public int getTotalScore() {
        return totalScore;
    }

    public void calculateTotalScore() {
        totalScore = 0;
        totalScore += getTotalKnockDownScores();
        totalScore += getTotalSpareBonus();
        totalScore += getTotalStrikeBonus();
    }
}
